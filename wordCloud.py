# -*- codeing =utf-8 -*-
# @Time : 2020/12/14 21:05 
# @Author : 三千xc
# @File : wordCloud.py 
# @Software: PyCharm

#分词
import jieba
#绘图，数据可视化
from matplotlib import  pyplot as plt
#词云
from wordcloud import WordCloud
#图片处理
from PIL import  Image
#矩阵运算
import  numpy as np
#数据库
import sqlite3


#准备词云所需的文字
con = sqlite3.connect('movie.db')
cur = con.cursor()
sql = 'select instroduction from movie250'
data = cur.execute(sql)
text = ""
for item in data:
    text = text + item[0]
    # print(item[0])
cur.close()
con.close()

#分词
cut = jieba.cut(text)
string = '  '.join(cut)
# print(len(string))

#打开遮罩图片
img = Image.open(r'.\static\images\spiderman.jpg')
# 将图片转换为数组
img_array = np.array(img)
wc = WordCloud(
        background_color='white',
        mask = img_array,
        font_path="MFQingShu_Noncommercial-Regular.otf"
)
wc.generate_from_text(string)

#绘制图片
fig = plt.figure(1)
plt.imshow(wc)
plt.axis('off')

# plt.show()
plt.savefig(r'.\static\images\word.jpg')